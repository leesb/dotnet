﻿using System;
using System.Collections.Generic;
using ConsoleTP1.BusinessManagement;
using ConsoleTP1.BusinessManagement.Strategy;
using ConsoleTP1.DBO;

namespace ConsoleTP1
{
    internal class Program
    {

        private static IList<DBO.Atelier> InitList()
        {
            var microsoft = new Atelier
            {
                Name = "Microsoft",
                Description = "L’atelier",
                TrackMark = new [] {1.3m, 2.58m},
                VivaMark = new [] {5.1m, 10.2m}
            };

            var adobe = new Atelier
            {
                Name = "Adobe",
                Description = "Atelier adobe",
                TrackMark = new [] { 2.6m, 3.06m },
                VivaMark = new [] { 4.2m, 2.9m, 0m }
            };

            var opensource = new Atelier
            {
                Name = "Open Source",
                Description = "Atelier open source",
                TrackMark = new decimal[] { 6, 1, 4, 2 },
                VivaMark = new decimal[] { 4, 6, 2, 5 }
            };

            var adminsys = new Atelier
            {
                Name = "Admin Sytème",
                Description = "Atelier adminsys",
                TrackMark = new decimal[] { 2, 5, 2, 1 },
                VivaMark = new decimal[] { 3, 6, 10, 8 }
            };

            var foo = new Atelier
            {
                Name = "Foo",
                Description = "Atelier Foo",
                TrackMark = new decimal[] {1, 2},
                VivaMark = new decimal[] {3, 5}
            };

            var bar = new Atelier
            {
                Name = "Bar",
                Description = "Atelier Bar",
                TrackMark = new decimal[] {},
                VivaMark = new decimal[] {}
            };

            var MiCrOsOfT = new Atelier
            {
                Name = "MiCrOsOfT",
                Description = "L’atelier",
                TrackMark = new [] {1.6m, 7.2m},
                VivaMark = new [] {4.2m, 8.2m}
            };

            return new List<Atelier> {microsoft, MiCrOsOfT, adobe, opensource, adminsys, foo, bar};
        }



        private static void Main(string[] args)
        {
            var ateliers = InitList();

            foreach (var atelier in ateliers)
            {
                var manager = FactoryAtelier.GetAtelier(atelier.Name);
                Console.WriteLine("Presentation");
                Console.WriteLine(manager.Presentation(atelier));

                Console.WriteLine("Track mark; Viva mark : " + atelier);

                Console.WriteLine("Default strategy : " + manager.Calculate(atelier));
                manager.CalculateStrategy = new CalculateFirstSemester();
                Console.WriteLine("First semester strategy : " + manager.Calculate(atelier));
                manager.CalculateStrategy = new CalculateSecondSemester();
                Console.WriteLine("Second semester strategy : " + manager.Calculate(atelier));
                Console.WriteLine();
                Console.WriteLine("========");
            }
        }
    }
}
