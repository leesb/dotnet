﻿using System.Text;

namespace ConsoleTP1.DBO
{
    public class Atelier
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal[] TrackMark { get; set; }
        public decimal[] VivaMark { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();

            var trackMark = string.Join(" ", TrackMark);
            var vivaMark = string.Join(" ", VivaMark);
            builder.Append(trackMark);
            builder.Append(" ;");
            builder.Append(vivaMark);
            builder.Append(" ");

            return builder.ToString();
        }
    }
}
