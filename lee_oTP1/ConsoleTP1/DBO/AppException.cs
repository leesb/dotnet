﻿using System;

namespace ConsoleTP1.DBO
{
    public class AppException : Exception
    {
        public AppException(string message, Exception e)
            : base(message, e)
        {
        }
    }
}
