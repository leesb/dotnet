﻿namespace ConsoleTP1.BusinessManagement
{
    public interface ICalculateStrategy
    {
        decimal CalculateAverage(DBO.Atelier atelier);
    }
}
