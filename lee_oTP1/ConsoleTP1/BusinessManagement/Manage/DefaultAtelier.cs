﻿using System;
using System.Linq;
using ConsoleTP1.DBO;

namespace ConsoleTP1.BusinessManagement.Manage
{
    public class DefaultAtelier : IAtelier
    {
        public ICalculateStrategy CalculateStrategy { get; set; }

        public decimal Calculate(Atelier atelier)
        {
            if (CalculateStrategy != null)
                return CalculateStrategy.CalculateAverage(atelier);

            var trackMarkAvg = atelier.TrackMark.Any() ? atelier.TrackMark.Sum() / atelier.TrackMark.Length : 0;
            var vivaMarkAvg = atelier.VivaMark.Any() ? atelier.VivaMark.Sum() / atelier.VivaMark.Length : 0;
            return (trackMarkAvg + vivaMarkAvg) / 2;
        }

        public virtual string Presentation(Atelier atelier)
        {
            return $"Nom : {atelier.Name}" +
                   Environment.NewLine +
                   Environment.NewLine +
                   Environment.NewLine +
                   $"Description : {atelier.Description}";
        }
    }
}
