﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTP1.DBO;

namespace ConsoleTP1.BusinessManagement.Manage
{
    public class OtherTechno : DefaultAtelier
    {
        public override string Presentation(Atelier atelier)
        {
            return base.Presentation(atelier)
                   + Environment.NewLine
                   + "Url : http://mti.epita.fr";
        }
    }
}
