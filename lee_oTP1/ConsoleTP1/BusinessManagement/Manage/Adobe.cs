﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTP1.DBO;

namespace ConsoleTP1.BusinessManagement.Manage
{
    public class Adobe : DefaultAtelier
    {
        public override string Presentation(Atelier atelier)
        {
            return base.Presentation(atelier) 
                   + Environment.NewLine
                   + "Url : http://adobe.com";
        }
    }
}
