﻿using System.Linq;
using ConsoleTP1.DBO;

namespace ConsoleTP1.BusinessManagement.Strategy
{
    public class CalculateSecondSemester : ICalculateStrategy
    {
        public decimal CalculateAverage(Atelier atelier)
        {
            const int trackMark = 2;
            const int vivaMark = 1;
            var avg = 0m;

            var total = 0;

            if (atelier.TrackMark.Any())
            {
                total += trackMark;
                avg += atelier.TrackMark.Sum() * trackMark / atelier.TrackMark.Length;
            }

            if (atelier.VivaMark.Any())
            {
                total += vivaMark;
                avg += atelier.VivaMark.Sum() * vivaMark / atelier.VivaMark.Length;
            }

            return total > 0 ? avg / total : 0;
        }
    }
}
