﻿using System;
using ConsoleTP1.BusinessManagement.Manage;

namespace ConsoleTP1.BusinessManagement
{
    public class FactoryAtelier
    {
        private static bool Compare(string first, string second)
        {
            return first.Equals(second, StringComparison.InvariantCultureIgnoreCase);
        }

        public static IAtelier GetAtelier(string typeName)
        {
            if (Compare("microsoft", typeName))
                return new Manage.Microsoft();
            if (Compare("adobe", typeName))
                return new Adobe();
            if (Compare("opensource", typeName) || Compare("adminsys", typeName))
                return new OtherTechno();
            return new DefaultAtelier();
        }
    }
}
