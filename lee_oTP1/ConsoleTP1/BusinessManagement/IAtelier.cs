﻿namespace ConsoleTP1.BusinessManagement
{
    public interface IAtelier
    {
        ICalculateStrategy CalculateStrategy { get; set; }
        decimal Calculate(DBO.Atelier atelier);
        string Presentation(DBO.Atelier atelier);
    }
}
