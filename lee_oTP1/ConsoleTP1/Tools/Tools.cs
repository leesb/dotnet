﻿using System;

namespace ConsoleTP1.Tools
{
    public class Tools
    {
        public static void PrintArray<E>(E[] inputArray)
        {
            foreach (var e in inputArray)
            {
                Console.Write(e);
                Console.Write(" ");
            }
            Console.WriteLine(Environment.NewLine);
        }

    }
}
