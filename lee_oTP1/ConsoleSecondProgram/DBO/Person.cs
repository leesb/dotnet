﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSecondProgram.DBO
{
    public class Person
    {
        public string Address { get; set; }
        public string Firstname { get; set; }
        public string Name { get; set; }
    }
}
