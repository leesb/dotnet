﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleSecondProgram.DBO;

namespace ConsoleSecondProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            var expected = new List<DBO.Person>
            {
                new Person {Name = "Nom", Firstname = "Prénom", Address = "Adresse"},
                new Person {Name = "Nom2", Firstname = "Prénom2", Address = "Adresse2"}
            };

            var result = BusinessManagement.Person.ReadData("example.csv");

            if (expected.Count != result.Count) return;
            Console.WriteLine(expected.Where((e, i) => !e.Name.Equals(result[i].Name)
                                                       || !e.Firstname.Equals(result[i].Firstname)
                                                       || !e.Address.Equals(result[i].Address)).Any()
                ? "Les éléments ne sont pas correctes"
                : "Les éléments sont correctes");
        }
    }
}
