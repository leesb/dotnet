﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSecondProgram.BusinessManagement
{
    public static class Person
    {
        public static List<DBO.Person> ReadData(string filename)
        {
            var reader = new DataAccess.ReadFile(filename);
            return reader.ReadData();
        }
    }
}
