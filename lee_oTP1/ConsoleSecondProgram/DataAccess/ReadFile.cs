﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleSecondProgram.DataAccess
{
    public class ReadFile : IDisposable
    {
        private StreamReader _sr;
        public ReadFile(string filename)
        {
            Init(filename);
        }

        ~ReadFile()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _sr?.Close();
                _sr?.Dispose();
            }
        }

        private void Init(string filename)
        {
            _sr = new StreamReader(filename);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<DBO.Person> ReadData()
        {
            var result = new List<DBO.Person>();

            string line;

            try
            {
                while ((line = _sr.ReadLine()) != null)
                {
                    var data = line.Split(';');
                    if (data.Length != 3)
                        throw new Exception();
                    var person = new DBO.Person {Name = data[0], Firstname = data[1], Address = data[2]};
                    result.Add(person);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erreur durant la lecture ");
            }
            finally
            {
                Dispose();
            }

            return result;
        }
    }
}
