﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleFirstProgram
{
    class Program
    {
        private static int Facto(int nb)
        {
            if (nb < 0)
            {
                return -1;
            }

            var result = 1;

            for (var i = 1; i <= nb; i++)
            {
                result *= i;
            }

            return result;
        }


        static void Main(string[] args)
        {
            Console.WriteLine(Facto(0));
            Console.WriteLine(Facto(1));
            Console.WriteLine(Facto(2));
            Console.WriteLine(Facto(5));
        }
    }
}
