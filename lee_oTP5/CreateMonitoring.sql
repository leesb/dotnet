USE [master]
GO
/****** Object:  Database [HomeMonitoring]     21:51:25 ******/
CREATE DATABASE [HomeMonitoring]
GO
USE [HomeMonitoring]
GO
/****** Object:  Table [dbo].[T_Device]     21:48:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Device](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[idRoom] [bigint] NOT NULL,
 CONSTRAINT [PK_T_Device] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Result]     21:48:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Result](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[idDevice] [bigint] NOT NULL,
	[value] [int] NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [PK_T_Result] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_Room]     21:48:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Room](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_T_Room] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[RoomAvg]     21:48:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RoomAvg]
AS
SELECT        dbo.T_Device.idRoom, AVG(dbo.T_Result.value) AS Average
FROM            dbo.T_Device INNER JOIN
                         dbo.T_Result ON dbo.T_Device.id = dbo.T_Result.idDevice INNER JOIN
                         dbo.T_Room ON dbo.T_Device.idRoom = dbo.T_Room.id
GROUP BY dbo.T_Device.idRoom

GO
ALTER TABLE [dbo].[T_Device]  WITH CHECK ADD  CONSTRAINT [FK_T_Device_T_Room] FOREIGN KEY([idRoom])
REFERENCES [dbo].[T_Room] ([id])
GO
ALTER TABLE [dbo].[T_Device] CHECK CONSTRAINT [FK_T_Device_T_Room]
GO
ALTER TABLE [dbo].[T_Result]  WITH CHECK ADD  CONSTRAINT [FK_T_Result_T_Device] FOREIGN KEY([idDevice])
REFERENCES [dbo].[T_Device] ([id])
GO
ALTER TABLE [dbo].[T_Result] CHECK CONSTRAINT [FK_T_Result_T_Device]
GO
