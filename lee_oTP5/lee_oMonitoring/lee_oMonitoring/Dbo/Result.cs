﻿using System;

namespace lee_oMonitoring.Dbo
{
    public class Result
    {
        public int Value { get; set; }
        public DateTime Date { get; set; }
        public long IdDevice { get; set; }
    }
}