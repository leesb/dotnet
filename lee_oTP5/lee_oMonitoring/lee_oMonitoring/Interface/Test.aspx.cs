﻿using System;
using System.IO;
using System.Web;
using lee_oMonitoring.DataAccess.Echange;

namespace lee_oMonitoring.Interface
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var filename = Path.Combine(HttpContext.Current.Server.MapPath("~"), "example.xml");
            var room = BusinessManagement.Room.ImportFromXml(filename, XmlMethod.XmlDocument);
            var room2 = BusinessManagement.Room.ImportFromXml(filename, XmlMethod.LinqToXml);
            BusinessManagement.Room.SaveToXml(room, method: XmlMethod.XmlDocument);
            BusinessManagement.Room.SaveToXml(room2, method: XmlMethod.LinqToXml);
        }
    }
}