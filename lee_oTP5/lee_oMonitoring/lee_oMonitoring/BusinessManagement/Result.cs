﻿namespace lee_oMonitoring.BusinessManagement
{
    public class Result
    {
        public static bool Add(Dbo.Result result)
        {
            var data = new DataAccess.Result();
            return data.Add(result);
        }

        public static long GetAverage(long idDevice)
        {
            var data = new DataAccess.Result();
            return data.GetAverage(idDevice);
        }
    }
}