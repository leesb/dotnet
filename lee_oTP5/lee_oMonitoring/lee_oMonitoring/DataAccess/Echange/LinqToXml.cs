﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Xml.Schema;

namespace lee_oMonitoring.DataAccess.Echange
{
    public class LinqToXml : IXmlEchange
    {
        private const string _schemaPath = "XmlSchema/XMLSchema.xsd";
        public List<Dbo.Room> ImportData(string xmlPath)
        {
            try
            {
                string schemaPath = Path.Combine(HttpContext.Current.Server.MapPath("~"), _schemaPath);

                XDocument tmpDoc = XDocument.Load(xmlPath);
                var schemas = new XmlSchemaSet();
                schemas.Add("", schemaPath);
                tmpDoc.Validate(schemas, null);

                return tmpDoc.Descendants("room").Select(room => new Dbo.Room {Name = room.Value}).ToList();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public string SaveData(List<Dbo.Room> rooms, string filename = null)
        {
            try
            {
                filename = filename ?? Path.Combine(Path.GetTempPath(), "out_linq_to_xml.xml");

                var tmpDoc = new XDocument(new XDeclaration("1.0", "utf-8", null),
                                           new XElement("Rooms",
                                                        rooms.Select(r => new XElement("room",
                                                                                       new XElement("name", r.Name)))));

                tmpDoc.Save(filename);
                return filename;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }
    }
}