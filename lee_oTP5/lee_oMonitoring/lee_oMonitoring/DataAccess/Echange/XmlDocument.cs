﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Xml;

namespace lee_oMonitoring.DataAccess.Echange
{
    public class XmlDocument : IXmlEchange
    {
        private const string _schemaPath = "XmlSchema/XMLSchema.xsd";

        public List<Dbo.Room> ImportData(string xmlPath)
        {
            try
            {
                string schemaPath = Path.Combine(HttpContext.Current.Server.MapPath("~"), _schemaPath);

                var tmpDoc = new System.Xml.XmlDocument();
                tmpDoc.Load(xmlPath);
                tmpDoc.Schemas.Add("", schemaPath);
                tmpDoc.Validate(null);
                XmlNodeList list = tmpDoc.SelectNodes("//room");

                if (list != null)
                {
                    var result = new List<Dbo.Room>();
                    foreach (XmlNode item in list)
                    {
                        result.Add(new Dbo.Room {Name = item.InnerText});
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public string SaveData(List<Dbo.Room> rooms, string filename = null)
        {
            try
            {
                filename = filename ?? Path.Combine(Path.GetTempPath(), "out_xml_document.xml");
                var tmpDoc = new System.Xml.XmlDocument();
                XmlDeclaration declaration = tmpDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                tmpDoc.InsertBefore(declaration, tmpDoc.DocumentElement);

                XmlElement root = tmpDoc.CreateElement("Rooms");
                XmlNode rootNode = tmpDoc.AppendChild(root);
                foreach (Dbo.Room room in rooms)
                {
                    XmlElement roomElement = tmpDoc.CreateElement("room");
                    XmlNode roomNode = rootNode.AppendChild(roomElement);
                    XmlElement nameElement = tmpDoc.CreateElement("name");
                    nameElement.InnerText = room.Name;
                    XmlNode nameNode = roomNode.AppendChild(nameElement);
                }

                tmpDoc.Save(filename);
                return filename;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }
    }
}