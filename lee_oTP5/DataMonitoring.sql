USE [HomeMonitoring]
GO
SET IDENTITY_INSERT [dbo].[T_Room] ON 
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (1, N'Box')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (2, N'Living')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (3, N'Hall')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (4, N'Office')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (5, N'Guest')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (6, N'Dining')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (7, N'Toilet')
GO
INSERT [dbo].[T_Room] ([id], [name]) VALUES (8, N'Bed')
GO
SET IDENTITY_INSERT [dbo].[T_Room] OFF
GO
SET IDENTITY_INSERT [dbo].[T_Device] ON 
GO
INSERT [dbo].[T_Device] ([id], [name], [idRoom]) VALUES (1, N'thermostat', 2)
GO
INSERT [dbo].[T_Device] ([id], [name], [idRoom]) VALUES (2, N'thermostat', 3)
GO
INSERT [dbo].[T_Device] ([id], [name], [idRoom]) VALUES (3, N'thermostat', 4)
GO
INSERT [dbo].[T_Device] ([id], [name], [idRoom]) VALUES (4, N'thermostat', 5)
GO
INSERT [dbo].[T_Device] ([id], [name], [idRoom]) VALUES (5, N'thermostat', 6)
GO
INSERT [dbo].[T_Device] ([id], [name], [idRoom]) VALUES (6, N'thermostat', 8)
GO
SET IDENTITY_INSERT [dbo].[T_Device] OFF
GO
SET IDENTITY_INSERT [dbo].[T_Result] ON 
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (1, 1, 15, CAST(N'2019-02-26T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (2, 2, 17, CAST(N'2019-02-26T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (3, 3, 16, CAST(N'2019-02-26T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (4, 4, 17, CAST(N'2019-02-26T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (5, 5, 17, CAST(N'2019-02-26T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (6, 6, 18, CAST(N'2019-02-26T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (9, 1, 20, CAST(N'2019-03-04T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (10, 2, 26, CAST(N'2019-03-04T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (11, 3, 22, CAST(N'2019-03-04T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (12, 4, 22, CAST(N'2019-03-04T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (13, 5, 28, CAST(N'2019-03-04T11:42:00.000' AS DateTime))
GO
INSERT [dbo].[T_Result] ([id], [idDevice], [value], [date]) VALUES (14, 6, 27, CAST(N'2019-03-04T11:42:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[T_Result] OFF
GO
