SET IDENTITY_INSERT [dbo].[Devices] ON 

INSERT [dbo].[Devices] ([id], [name], [idRoom]) VALUES (1, N'thermostat', 2)
INSERT [dbo].[Devices] ([id], [name], [idRoom]) VALUES (2, N'thermostat', 3)
INSERT [dbo].[Devices] ([id], [name], [idRoom]) VALUES (3, N'thermostat', 4)
INSERT [dbo].[Devices] ([id], [name], [idRoom]) VALUES (4, N'thermostat', 5)
INSERT [dbo].[Devices] ([id], [name], [idRoom]) VALUES (5, N'thermostat', 6)
INSERT [dbo].[Devices] ([id], [name], [idRoom]) VALUES (6, N'thermostat', 8)
SET IDENTITY_INSERT [dbo].[Devices] OFF
SET IDENTITY_INSERT [dbo].[Results] ON 

INSERT [dbo].[Results] ([id], [idDevice], [value], [date]) VALUES (1, 1, N'15', CAST(N'2019-02-26T11:42:00.000' AS DateTime))
INSERT [dbo].[Results] ([id], [idDevice], [value], [date]) VALUES (2, 2, N'17', CAST(N'2019-02-26T11:42:00.000' AS DateTime))
INSERT [dbo].[Results] ([id], [idDevice], [value], [date]) VALUES (3, 3, N'16', CAST(N'2019-02-26T11:42:00.000' AS DateTime))
INSERT [dbo].[Results] ([id], [idDevice], [value], [date]) VALUES (4, 4, N'17', CAST(N'2019-02-26T11:42:00.000' AS DateTime))
INSERT [dbo].[Results] ([id], [idDevice], [value], [date]) VALUES (5, 5, N'17', CAST(N'2019-02-26T11:42:00.000' AS DateTime))
INSERT [dbo].[Results] ([id], [idDevice], [value], [date]) VALUES (6, 6, N'18', CAST(N'2019-02-26T11:42:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Results] OFF
SET IDENTITY_INSERT [dbo].[Rooms] ON 

INSERT [dbo].[Rooms] ([id], [name]) VALUES (1, N'Box')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (2, N'Living')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (3, N'Hall')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (4, N'Office')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (5, N'Guest')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (6, N'Dining')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (7, N'Toilet')
INSERT [dbo].[Rooms] ([id], [name]) VALUES (8, N'Bed')
SET IDENTITY_INSERT [dbo].[Rooms] OFF