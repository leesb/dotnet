/****** Object:  Database [HomeMonitoring]    Script Date: 2019-02-26 12:03:32 PM ******/
CREATE DATABASE [HomeMonitoring]
GO
ALTER DATABASE [HomeMonitoring] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HomeMonitoring].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HomeMonitoring] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HomeMonitoring] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HomeMonitoring] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HomeMonitoring] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HomeMonitoring] SET ARITHABORT OFF 
GO
ALTER DATABASE [HomeMonitoring] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HomeMonitoring] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HomeMonitoring] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HomeMonitoring] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HomeMonitoring] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HomeMonitoring] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HomeMonitoring] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HomeMonitoring] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HomeMonitoring] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HomeMonitoring] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HomeMonitoring] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HomeMonitoring] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HomeMonitoring] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HomeMonitoring] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HomeMonitoring] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HomeMonitoring] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HomeMonitoring] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HomeMonitoring] SET RECOVERY FULL 
GO
ALTER DATABASE [HomeMonitoring] SET  MULTI_USER 
GO
ALTER DATABASE [HomeMonitoring] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HomeMonitoring] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HomeMonitoring] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HomeMonitoring] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [HomeMonitoring] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'HomeMonitoring', N'ON'
GO
ALTER DATABASE [HomeMonitoring] SET QUERY_STORE = OFF
GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 2019-02-26 12:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
USE [HomeMonitoring]
GO
CREATE TABLE [dbo].[Rooms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](64) NULL,
 CONSTRAINT [PK_Rooms] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Results]    Script Date: 2019-02-26 12:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDevice] [int] NULL,
	[value] [nvarchar](64) NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_Results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Devices]    Script Date: 2019-02-26 12:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Devices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](64) NULL,
	[idRoom] [int] NULL,
 CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[RoomTemperature]    Script Date: 2019-02-26 12:03:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RoomTemperature]
AS
SELECT        dbo.Results.value, dbo.Rooms.name
FROM            dbo.Devices INNER JOIN
                         dbo.Results ON dbo.Devices.id = dbo.Results.idDevice INNER JOIN
                         dbo.Rooms ON dbo.Devices.idRoom = dbo.Rooms.id
GO

ALTER TABLE [dbo].[Devices]  WITH CHECK ADD  CONSTRAINT [FK_Devices_Rooms] FOREIGN KEY([idRoom])
REFERENCES [dbo].[Rooms] ([id])
GO
ALTER TABLE [dbo].[Devices] CHECK CONSTRAINT [FK_Devices_Rooms]
GO
ALTER TABLE [dbo].[Results]  WITH CHECK ADD  CONSTRAINT [FK_Results_Devices] FOREIGN KEY([idDevice])
REFERENCES [dbo].[Devices] ([id])
GO
ALTER TABLE [dbo].[Results] CHECK CONSTRAINT [FK_Results_Devices]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Devices"
            Begin Extent = 
               Top = 139
               Left = 62
               Bottom = 252
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Results"
            Begin Extent = 
               Top = 43
               Left = 397
               Bottom = 173
               Right = 567
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Rooms"
            Begin Extent = 
               Top = 243
               Left = 393
               Bottom = 339
               Right = 563
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 1305
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'RoomTemperature'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'RoomTemperature'
GO
ALTER DATABASE [HomeMonitoring] SET  READ_WRITE 
GO
