﻿using System;
using System.Collections.Generic;
using System.Linq;
using lee_oMonitoring.DataAccess;

namespace lee_oMonitoring.BusinessManagement
{
    public static class DataClasses
    {


        public static int GetAverage(long id)
        {
            using (var dataContext = new DataClassesDataContext())
            {
                var query = dataContext.DeviceAverage(id);
                return query.FirstOrDefault()?.Column1 ?? -1; ;
            }
        }

        public static void AddDevice(string name, int idRoom)
        {
            using (var dataContext = new DataClassesDataContext())
            {
                if (dataContext.T_Rooms.Any(r => r.id == idRoom))
                {
                    var device = new T_Device { name = name, idRoom = idRoom };
                    dataContext.T_Devices.InsertOnSubmit(device);
                    dataContext.SubmitChanges();
                }
            }
        }

        public static List<T_Room> GetRooms()
        {
            using (var dataContext = new DataClassesDataContext())
            {
                return dataContext.T_Rooms.ToList();
            }
        }

        public static void AddResult(long idDevice, int value)
        {
            using (var dataContext = new DataClassesDataContext())
            {
                var result = new T_Result { date = DateTime.Now, idDevice = idDevice, value = value };
                dataContext.T_Results.InsertOnSubmit(result);
                dataContext.SubmitChanges();
            }
        }
    }
}