﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using lee_oMonitoring.DataAccess;

namespace lee_oMonitoring.Interface
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int average = BusinessManagement.DataClasses.GetAverage(2);
            BusinessManagement.DataClasses.AddDevice("toto", 2);
            List<T_Room> rooms = BusinessManagement.DataClasses.GetRooms();
            BusinessManagement.DataClasses.AddResult(1, 42);
        }
    }
}