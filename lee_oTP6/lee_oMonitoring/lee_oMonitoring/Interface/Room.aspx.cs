﻿using System;
using System.Web.UI.WebControls;

namespace lee_oMonitoring.Interface
{
    public partial class Room : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh();
            }
        }

        private void Refresh()
        {

            BulletedList1.DataTextField = "Name";
            BulletedList1.DataSource = BusinessManagement.Room.GetList();
            BulletedList1.DataBind();
        }

        protected void BulletedList1_Click(object sender, BulletedListEventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Dbo.Room room = new Dbo.Room { Name = TextBox1.Text };
            if (BusinessManagement.Room.Add(room))
            {
                Refresh();
            }
        }
    }
}