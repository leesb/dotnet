﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;

namespace lee_oMonitoring.Interface
{
    public partial class Device : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh();
            }
        }

        private void Refresh()
        {
            List<Dbo.Room> rooms = BusinessManagement.Room.GetList();
            List<Dbo.Device> devices = BusinessManagement.Device.GetList();

            var data = rooms.Select(r => new
                                         {
                                             Room = $"{r.Name} ({r.Id})",
                                             Devices = string.Join(", ", devices.Where(d => d.IdRoom == r.Id).Select(d => $"{d.Name} ({d.Id})" ))
                                         });

            GridView1.DataSource = data;
            GridView1.DataBind();
            BindData(DropDownList1, rooms, "Name", "Id");
            BindData(BulletedList1, devices, "Name", "Id");
        }

        private void BindData(ListControl control, object source, string field, string value = null)
        {
            control.DataTextField = field;
            control.DataValueField = value;
            control.DataSource = source;
            control.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            long.TryParse(DropDownList1.SelectedValue, out long id);
            if (BusinessManagement.Room.Get(id) != null)
            {
                if (!TextBox1.Text.IsNullOrWhiteSpace())
                {
                    var item = new Dbo.Device { IdRoom = id, Name = TextBox1.Text };
                    if (BusinessManagement.Device.Add(item))
                    {
                        Refresh();
                    }
                }
            }
        }
    }
}