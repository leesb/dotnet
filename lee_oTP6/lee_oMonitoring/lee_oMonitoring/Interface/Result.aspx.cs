﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace lee_oMonitoring.Interface
{
    public partial class Result : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh();
            }
        }
        private void Refresh()
        {
            List<Dbo.Room> rooms = BusinessManagement.Room.GetList();
            List<Dbo.Device> devices = BusinessManagement.Device.GetList();

            var data = rooms.Select(r => new
                                         {
                                             Room = $"{r.Name} ({r.Id})",
                                             Devices = string.Join("\n", devices.Where(d => d.IdRoom == r.Id).Select(d => $"{d.Name}#{d.Id} : {BusinessManagement.Result.GetAverage(d.Id)}"))
                                         });
            
            GridView1.DataSource = data;
            GridView1.DataBind();

            DropDownList1.DataSource = devices.Select(d => new{ Name = $"{d.Name} ({d.Id})", Id = d.Id });
            DropDownList1.DataTextField = "Name";
            DropDownList1.DataValueField = "Id";
            DropDownList1.DataBind();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Text = e.Row.Cells[1].Text.Replace("\n", "<br/>");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool pass = int.TryParse(TextBox1.Text, out int value);
            pass &= long.TryParse(DropDownList1.SelectedValue, out long id);

            if (pass && id >= 0)
            {
                var result = new Dbo.Result {Date = DateTime.Now, IdDevice = id, Value = value};
                if (BusinessManagement.Result.Add(result))
                {
                    Refresh();
                }
            }
        }
    }
}