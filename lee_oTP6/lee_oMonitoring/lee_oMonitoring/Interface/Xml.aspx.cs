﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace lee_oMonitoring.Interface
{
    public partial class Xml : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                try
                {
                    string filename = Server.MapPath("~/Uploaded/") + FileUpload1.FileName;
                    FileUpload1.SaveAs(filename);
                    List<Dbo.Room> data = BusinessManagement.Room.ImportFromXml(filename);
                    if (data == null)
                    {
                        throw new InvalidDataException();
                    }
                    else
                    {
                        data.ForEach(d => BusinessManagement.Room.Add(d));
                        Label2.Text = "Added successfully";
                        Label2.ForeColor = Color.Green;
                    }
                }
                catch (Exception ex)
                {
                    string msg = "An error occured";
                    Debug.WriteLine(ex.Message);
                    Label2.Text = msg;
                    Label2.ForeColor = Color.Crimson;
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string filename = BusinessManagement.Room.SaveToXml(BusinessManagement.Room.GetList());
            try
            {
                byte[] stream = File.ReadAllBytes(filename);
                Response.Clear();
                Response.ContentType = "text/xml";
                Response.AddHeader("Content-Disposition", "attachment; filename=Rooms.xml");
                Response.OutputStream.Write(stream, 0, stream.Length);
                Response.End();
            }
            catch (Exception ex)
            {
                string msg = "An error occured";
                Debug.WriteLine(ex.Message);
                Label2.Text = msg;
                Label2.ForeColor = Color.Crimson;
            }
        }
    }
}