﻿using System.Collections.Generic;

namespace lee_oMonitoring.DataAccess.Echange
{
    public interface IXmlEchange
    {
        List<Dbo.Room> ImportData(string xmlPath);
        string SaveData(List<Dbo.Room> rooms, string filename = null);
    }
}
