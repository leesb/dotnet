﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lee_oMonitoring.DataAccess
{
    public class Device
    {
        public Dbo.Device Get(long id)
        {
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    var device = ctx.T_Devices.FirstOrDefault(x => x.id == id);
                    if (device != null)
                    {
                        return new Dbo.Device()
                        {
                            Id = device.id,
                            Name = device.name
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                // log exception
                return null;
            }
        }

        public List<Dbo.Device> GetList()
        {
            var res = new List<Dbo.Device>();
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    foreach (var device in ctx.T_Devices)
                    {
                        res.Add(new Dbo.Device()
                        {
                            Id = device.id,
                            Name = device.name,
                            IdRoom = device.idRoom
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                return res;
            }
            return res;
        }

        public bool Add(Dbo.Device device)
        {
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    ctx.T_Devices.InsertOnSubmit(new T_Device()
                    {
                        name = device.Name,
                        idRoom = device.IdRoom
                    });
                    ctx.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}