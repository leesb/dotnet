﻿using System;
using System.Linq;

namespace lee_oMonitoring.DataAccess
{
    public class Result
    {
        public bool Add(Dbo.Result result)
        {
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    ctx.T_Results.InsertOnSubmit(new T_Result()
                                                 {
                                                     idDevice = result.IdDevice,
                                                     value = result.Value,
                                                     date = result.Date
                                                 });
                    ctx.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                // log exception
                return false;
            }
        }

        public long GetAverage(long idDevice)
        {
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    var res = ctx.DeviceAverage(idDevice);
                    return res.FirstOrDefault()?.Column1 ?? -1;
                }
            }
            catch (Exception ex)
            {
                // log exception
                return -1;
            }
        }
    }
}