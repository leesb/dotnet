﻿using System;
using System.Collections.Generic;
using System.Linq;
using lee_oMonitoring.DataAccess.Echange;

namespace lee_oMonitoring.DataAccess
{
    public class Room
    {
        private IXmlEchange _echange = new XmlDocument();
        private XmlMethod _method;
        public XmlMethod Method
        {
            get => _method;
            set
            {
                _method = value;
                if (Method == XmlMethod.XmlDocument)
                    _echange = new XmlDocument();
                if (Method == XmlMethod.LinqToXml)
                    _echange = new LinqToXml();
            }

        }

        public Dbo.Room Get(long id)
        {
            using (DataClassesDataContext ctx = new DataClassesDataContext())
            {
                try
                {
                    var room = ctx.T_Rooms.FirstOrDefault(x => x.id == id);
                    if (room != null)
                    {
                        return new Dbo.Room()
                        {
                            Id = room.id,
                            Name = room.name
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    // log exception
                    return null;
                }
            }
        }

        public List<Dbo.Room> GetList()
        {
            var res = new List<Dbo.Room>();
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    foreach (var room in ctx.T_Rooms)
                    {
                        res.Add(new Dbo.Room()
                        {
                            Id = room.id,
                            Name = room.name
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                // log exception
            }
            return res;
        }

        public bool Add(Dbo.Room room)
        {
            try
            {
                using (DataClassesDataContext ctx = new DataClassesDataContext())
                {
                    ctx.T_Rooms.InsertOnSubmit(new T_Room()
                    {
                        name = room.Name
                    });
                    ctx.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                //log exception
                return false;
            }
        }

        public List<Dbo.Room> ImportFromXml(string filename)
        {
            return _echange.ImportData(filename);
        }

        public string SaveToXml(List<Dbo.Room> rooms, string filename = null)
        {
            return _echange.SaveData(rooms, filename);
        }
    }
}