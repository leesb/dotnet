﻿using System.Collections.Generic;

namespace lee_oMonitoring.BusinessManagement
{
    public class Device
    {
        public static Dbo.Device Get(long id)
        {
            var data = new DataAccess.Device();
            return data.Get(id);
        }

        public static List<Dbo.Device> GetList()
        {
            var data = new DataAccess.Device();
            return data.GetList();
        }

        public static bool Add(Dbo.Device device)
        {
            var data = new DataAccess.Device();
            return data.Add(device);
        }
    }
}