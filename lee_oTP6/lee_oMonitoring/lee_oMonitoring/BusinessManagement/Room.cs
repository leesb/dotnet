﻿using System.Collections.Generic;
using lee_oMonitoring.DataAccess.Echange;

namespace lee_oMonitoring.BusinessManagement
{
    public class Room
    {
        public static Dbo.Room Get(long id)
        {
            var data = new DataAccess.Room();
            return data.Get(id);
        }
        public static List<Dbo.Room> GetList()
        {
            var data = new DataAccess.Room();
            return data.GetList();
        }

        public static bool Add(Dbo.Room room)
        {
            var data = new DataAccess.Room();
            return data.Add(room);
        }

        public static List<Dbo.Room> ImportFromXml(string filename, XmlMethod method = XmlMethod.XmlDocument)
        {
            var data = new DataAccess.Room { Method = method };
            return data.ImportFromXml(filename);
        }

        public static string SaveToXml(List<Dbo.Room> rooms, string filename = null, XmlMethod method = XmlMethod.XmlDocument)
        {
            var data = new DataAccess.Room { Method = method };
            return data.SaveToXml(rooms, filename);
        }
    }
}