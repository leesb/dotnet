﻿namespace lee_oMonitoring.Dbo
{
    public class Device
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long IdRoom { get; set; }
    }
}