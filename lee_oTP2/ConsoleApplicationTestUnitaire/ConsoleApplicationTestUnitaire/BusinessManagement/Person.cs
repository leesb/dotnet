﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplicationTestUnitaire.BusinessManagement
{
    public static class Person
    {
       public static bool SavePerson(DBO.Person person)
       {
           return DataAccess.Person.SavePerson(person);
       }

       public static DBO.Person GetPerson(int id)
       {
           return DataAccess.Person.GetPerson(id);
       }
    }
}
