﻿using System;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tester.DataAccess
{
    [TestClass]
    public class LogMeTester
    {
        [TestMethod]
        public void WhenLogging_ThenLogMustNotThrowDirectoryNotFoundException()
        {
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTime.NowGet = () => new DateTime(2019, 2, 25, 12, 45, 0);
                System.Fakes.ShimDateTime.TodayGet = () => new DateTime(2019, 2, 25);

                try
                {
                    ConsoleApplicationTestUnitaire.DataAccess.LogMe.Log(new Exception("test"));
                }
                catch (Exception)
                {
                    Assert.Fail("Logger must not throw an exception");
                }
            }
        }

        [TestMethod]
        public void WhenLogging_ThenLogFileMustExist()
        {
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTime.NowGet = () => new DateTime(2019, 2, 25, 12, 45, 0);
                System.Fakes.ShimDateTime.TodayGet = () => new DateTime(2019, 2, 25);

                ConsoleApplicationTestUnitaire.DataAccess.LogMe.Log(new Exception("test"));
                var filename = DateTime.Today.ToShortDateString().Replace('/', '_');
                Assert.IsTrue(System.IO.File.Exists(filename));
            }
        }
    }
}
