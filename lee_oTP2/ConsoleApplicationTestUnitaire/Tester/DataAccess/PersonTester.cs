﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tester.DataAccess
{
    [TestClass]
    public class PersonTester
    {
        public TestContext TestContext { get; set; }

        [DeploymentItem("Person.xml"),
         DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                    "|DataDirectory|\\Person.xml",
                    "person",
                    DataAccessMethod.Sequential),
         TestMethod()]
        public void GivenId_WhenGetPerson_ThenReturnThatPerson()
        {
            var id = int.Parse(TestContext.DataRow["id"].ToString());
            var expected = new ConsoleApplicationTestUnitaire.DBO.Person
            {
                Address = TestContext.DataRow["address"].ToString(),
                Firstname = TestContext.DataRow["firstname"].ToString(),
                Name = TestContext.DataRow["name"].ToString(),
                Function = TestContext.DataRow["function"].ToString()
            };

            var result = ConsoleApplicationTestUnitaire.DataAccess.Person.GetPerson(id);

            Assert.AreEqual(expected.Firstname, result.Firstname);
            Assert.AreEqual(expected.Name, result.Name);
            Assert.AreEqual(expected.Address, result.Address);
            Assert.AreEqual(expected.Function, result.Function);
        }


        [DeploymentItem("Person.xml"),
         DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
             "|DataDirectory|\\Person.xml",
             "person",
             DataAccessMethod.Sequential),
         TestMethod()]
        public void GivenAPerson_WhenSaving_ThenReturnTrue()
        {
            var input = new ConsoleApplicationTestUnitaire.DBO.Person
            {
                Address = TestContext.DataRow["address"].ToString(),
                Firstname = TestContext.DataRow["firstname"].ToString(),
                Name = TestContext.DataRow["name"].ToString(),
                Function = TestContext.DataRow["function"].ToString()
            };

            var result = ConsoleApplicationTestUnitaire.DataAccess.Person.SavePerson(input);

            Assert.IsTrue(result);
        }
    }
}
