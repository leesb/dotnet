﻿using System;
using ConsoleApplicationTestUnitaire.DBO;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tester.BusinessManagement
{
    [TestClass]
    public class PersonTester
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void WhenGetExistingPersonId_ThenReturnThatPerson()
        {
            using (ShimsContext.Create())
            {
                ConsoleApplicationTestUnitaire.DataAccess.Fakes.ShimPerson.GetPersonInt32 =
                    (id) => id == 0 ? new ConsoleApplicationTestUnitaire.DBO.Person() : null;

                var person = ConsoleApplicationTestUnitaire.BusinessManagement.Person.GetPerson(0);

                Assert.IsNotNull(person);
            }
        }

        [TestMethod]
        public void WhenGetNegativeId_ThenReturnNull()
        {
            using (ShimsContext.Create())
            {
                ConsoleApplicationTestUnitaire.DataAccess.Fakes.ShimPerson.GetPersonInt32 =
                    (id) => id >= 0 ? new ConsoleApplicationTestUnitaire.DBO.Person() : null;

                var person = ConsoleApplicationTestUnitaire.BusinessManagement.Person.GetPerson(-42);

                Assert.IsNull(person);
            }
        }

        [DeploymentItem("Person.xml"),
         DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
             "|DataDirectory|\\Person.xml",
             "person",
             DataAccessMethod.Sequential),
         TestMethod()]
        public void WhenSaving_ThenItMustNotThrowAndError()
        {
            using (ShimsContext.Create())
            {
                ConsoleApplicationTestUnitaire.DataAccess.Fakes.ShimPerson.SavePersonPerson =
                    (p) => throw new Exception("error");

                var person = new Person
                {
                    Address = TestContext.DataRow["address"]?.ToString(),
                    Firstname = TestContext.DataRow["firstname"]?.ToString(),
                    Function = TestContext.DataRow["function"]?.ToString(),
                    Name = TestContext.DataRow["name"]?.ToString()
                };

                try
                {
                    var result = ConsoleApplicationTestUnitaire.BusinessManagement.Person.SavePerson(person);
                }
                catch (Exception)
                {
                    Assert.Fail("Saving must not throw an exception");
                }
            }
        }
    }
}
