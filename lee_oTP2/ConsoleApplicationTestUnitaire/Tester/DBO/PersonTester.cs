﻿using ConsoleApplicationTestUnitaire.DBO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tester.DBO
{
    [TestClass]
    public class PersonTester
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void WhenCreatePerson_ThenPropertiesAreInitializedWithEmptyString()
        {
            var result = new Person();
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.Address);
            Assert.AreEqual("", result.Firstname);
            Assert.AreEqual("", result.Function);
            Assert.AreEqual("", result.Name);
        }

        [DeploymentItem("Person.xml"),
         DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
             "|DataDirectory|\\Person.xml",
             "person",
             DataAccessMethod.Sequential),
         TestMethod()]
        public void WhenSetProperties_ThenPropertiesAreCorrectlySet()
        {
            var function = TestContext.DataRow["function"].ToString();
            var name = TestContext.DataRow["name"].ToString();
            var firstname = TestContext.DataRow["firstname"].ToString();
            var address = TestContext.DataRow["address"].ToString();

            var result = new Person { Address = address, Function = function, Firstname = firstname, Name = name };

            Assert.IsNotNull(result);
            Assert.AreEqual(firstname, result.Firstname);
            Assert.AreEqual(name, result.Name);
            Assert.AreEqual(function, result.Function);
            Assert.AreEqual(address, result.Address);
        }
    }
}
